Python-CAT9532
--------------
This Python module is intended to control the CAT9532 IO expander over I2C.
It is based on the Python smbus2(https://pypi.org/project/smbus2/) module.

The CAT9532 is a 16-channel I2C IO-expander with open drain outputs.
Its open drain outputs are ideal to drive LEDs, but can also act as GPIOs.
Because the driver is made with driving LEDs in mind, it also supports LED blinking.
It is possible to set a blinking period and duty cycle and let the driver blink an LED.
There are two blinking periods and duty cycles which can be set. Every LED can be
set individually to blink according to one of the two configured periods/duty cycles.

Usage
-----
This module is very basic and simple.
Eveyrhting is written with simplicity in mind.

Example code:
```
import cat9532
# Create the object
io_exp = cat9532.cat9532()
# Set pin0 as input
io_exp.pin_function(pin=0, function=cat9532.INPUT)
# Set pin1 as output
io_exp.pin_function(pin=1, function=cat9532.OUTPUT, default=cat9532.HIGH)
# Set output1 to blink with period0/duty_cycle0
io_exp.set_output(out_pin=1, state=cat9532.BLINK0)
# Set a new period/duty_cycle for BLINK0
io_exp.blink0_period = 0.5
io_exp.blink0_duty_cycle = 20
# Now read out the values which are actually set
print(io_exp.blink0_period, io_exp.blink0_duty_cycle)
```

Platforms
---------
The module was written and tested on the Raspberry Pi.
