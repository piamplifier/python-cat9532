""" Python-CAT9532
    Copyright (C) 2021  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Module to control the CAT9532 I2C IO-expander.

    The CAT9532 is an I2C IO-expander sepcifically made
    to drive LEDs. The open drain outputs can also be
    used in conjunction with a pull-up resistor to 
    form a GPIO.

    This library is based on the Python smbus2 module
    and was tested on a Raspberry Pi.

    Example:
        import cat9532
        # Create a new object.
        io_exp = cat9532.cat9532()
        # Set pin0 as input
        io_exp.pin_function(pin=0, function=INPUT)
        # Set pin1 as output
        io_exp.pin_function(pin=1, function=cat9532.OUTPUT, default=cat9532.HIGH)

"""
import smbus2
import threading

# Constants
INPUT = 0xEF
OUTPUT = 0xFF
HIGH = 0x00
LOW = 0x01
BLINK0 = 0x02
BLINK1 = 0x03


class cat9532():
    """
    """
    base_address = 0xC0 >> 1
    reg = {'Input0': 0x0, 'Input1': 0x1, 'Psc0': 0x2, 'Pwm0': 0x3,
           'Psc1': 0x4, 'Pwm1': 0x5, 'Ls0': 0x6, 'Ls1': 0x7,
           'Ls2': 0x8, 'Ls3': 0x9}
    led_select = ['Ls0', 'Ls1', 'Ls2', 'Ls3']

    def __init__(self, address_select=0):
        self.i2c_intf = smbus2.SMBus(1)
        self.lock = threading.Lock()

        self._address = address_select | self.base_address
        # Read out the current values.
        self._duty_cycle = [0, 0]
        self.blink0_duty_cycle
        self.blink1_duty_cycle

        self._blink_period = [0, 0]
        self.blink0_period
        self.blink1_period

    def pin_function(self, pin, function, default=LOW):
        """ Set the function of a certain pin.

            The function of every pin can be either input or output.
            Note that the outputs are open drain. So driving the
            output high actually means making it high impedant.

            :param: pin         Which pin to configure. 0-15
            :param: function    Set the output either as INPUT or OUTPUT
            :param: default     This parameter is only used when the pin
                                is set as an output. It is used to determine
                                the state of the pin after configuring it as
                                an output. It can be either HIGH, LOW or
                                blinking at rate BLINK1 or BLINK0.
        """
        assert pin >= 0 and pin <= 15, f"Wrong pin value: {pin}. Must be between 0-15"
        assert function == INPUT or function == OUTPUT, f"Function value must be either INPUT or OUPUT."
        assert default == LOW or default == HIGH or default == BLINK1 or default == BLINK0, \
            f"Default pin state must be LOW, HIGH, BLINK1 or BLINK0."

        pin_state = 0x00
        # If the function is INPUT, set the pin in HiZ
        # If the function is OUTPUT, set the pin to the default state
        if function == INPUT:
            pin_state = 0x00
        else:
            pin_state = default

        # Get the right led select register
        led_sel = self.led_select[int(pin/4)]

        self.lock.acquire()
        # Get the old register value and construct the new one
        reg_val = self.i2c_intf.read_byte_data(
            self._address, self.reg[led_sel])
        shift = int(2 * (pin - (int(pin / 4) * 4)))
        reg_val = reg_val & ~(0x3 << shift)
        reg_val = reg_val | (pin_state << shift)
        self.i2c_intf.write_byte_data(
            self._address, self.reg[led_sel], reg_val)
        self.lock.release()

    def set_output(self, out_pin, state):
        """ Set the state of an output pin.

            :param: out_pin The selected output pin. 0-15
            :param: state   Selected state of the pin.
                            :see: pin_function() default parameter
        """
        self.pin_function(out_pin, OUTPUT, state)

    def get_inputs(self):
        """ Read out the state of all the inputs.

            Function returns an representing the current input
            states. 1 = 'HIGH', 0 = 'LOW'
            MSB = IN15, LSB = IN0

            Note that the state of a pin can be read out even
            if it is used as an output. In this case the state
            of the pin should match the actual state.
        """
        self.lock.acquire()
        input_state = self.i2c_intf.read_word_data(
            self._address, self.reg['Input0'] | 0x10)
        self.lock.release()

        return input_state

    def get_input_state(self, in_pin):
        """ Get the state of one specific input pin.

            :param: in_pin  The input pin for which to return the
                            state. Value needs to be an integer
                            between 0 and 15

            :see: get_inputs()
        """
        assert in_pin >= 0 and in_pin <= 15, f"Wrong input pin value: {in_pin}. Needs to be 0-15"

        self.lock.acquire()
        if in_pin > 7:
            reg_val = self.i2c_intf.read_byte_data(
                self._address, self.reg['Input1'])
            reg_val = (reg_val >> (15 - in_pin)) & 0x1
        else:
            reg_val = self.i2c_intf.read_byte_data(
                self._address, self.reg['Input0'])
            reg_val = (reg_val >> in_pin) & 0x1
        self.lock.release()

        return reg_val

    @property
    def blink0_duty_cycle(self):
        """ Get the current blink0 duty_cycle.
        """
        self.lock.acquire()
        reg_val = self.i2c_intf.read_byte_data(
            self._address, self.reg['Pwm1'])
        self.lock.release()

        self._duty_cycle[0] = (reg_val / 255.0) * 100
        return self._duty_cycle[0]

    @blink0_duty_cycle.setter
    def blink0_duty_cycle(self, duty_cycle):
        """ Set the desired blinking duty cycle for blink1.

            :param: duty_cycle  A duty cycle value between 0 and 100.
        """
        assert duty_cycle >= 0 and duty_cycle <= 100, \
            f"Wrong duty_cycle value: {duty_cycle}. Must be between 0 and 100%."

        # Pwm value must be between 0 and 255.
        reg_val = round(255 * (duty_cycle / 100.0))
        self._duty_cycle[0] = (reg_val / 255.0) * 100

        self.lock.acquire()
        self.i2c_intf.write_byte_data(
            self._address, self.reg['Pwm0'], reg_val)
        self.lock.release()

    @property
    def blink1_duty_cycle(self):
        """ Get the current blink1 duty_cycle.
        """
        self.lock.acquire()
        reg_val = self.i2c_intf.read_byte_data(
            self._address, self.reg['Pwm1'])
        self.lock.release()

        self._duty_cycle[1] = (reg_val / 255.0) * 100
        return self._duty_cycle[1]

    @blink1_duty_cycle.setter
    def blink1_duty_cycle(self, duty_cycle):
        """ Set the desired blinking duty cycle for blink1.

            :param: duty_cycle  A duty cycle value between 0 and 100.
        """
        assert duty_cycle >= 0 and duty_cycle <= 100, \
            f"Wrong duty_cycle value: {duty_cycle}. Must be between 0 and 100%."

        # Pwm value must be between 0 and 255.
        reg_val = round(255 * (duty_cycle / 100.0))
        self._duty_cycle[1] = (reg_val / 255.0) * 100

        self.lock.acquire()
        self.i2c_intf.write_byte_data(
            self._address, self.reg['Pwm1'], reg_val)
        self.lock.release()

    @property
    def blink0_period(self):
        """ Get the current blinking0 period.
        """
        self.lock.acquire()
        reg_val = self.i2c_intf.read_byte_data(
            self._address, self.reg['Psc0'])
        self.lock.release()

        self._blink_period[0] = (reg_val + 1) / 152.0
        return self._blink_period[0]

    @blink0_period.setter
    def blink0_period(self, period):
        """ Set the blink0 period.

            :param: period  Period value between 6.58 ms and 1.69 s.
        """
        assert period >= 6.58e-3 and period <= 1.69, \
            f"Wrong period value: {period}. Value must be between {6.58e-3} and {1.69}"

        reg_val = int((period * 152.0) - 1)
        self._blink_period[0] = (reg_val + 1) / 152.0

        self.lock.acquire()
        self.i2c_intf.write_byte_data(
            self._address, self.reg['Psc0'], reg_val)
        self.lock.release()

    @property
    def blink1_period(self):
        """ Get the current blinking1 period.
        """
        self.lock.acquire()
        reg_val = self.i2c_intf.read_byte_data(
            self._address, self.reg['Psc1'])
        self.lock.release()

        self._blink_period[1] = (reg_val + 1) / 152.0
        return self._blink_period[1]

    @blink1_period.setter
    def blink1_period(self, period):
        """ Set the blink1 period.

            :param: period  Period value between 6.58 ms and 1.69 s.
        """
        assert period >= 6.58e-3 and period <= 1.69, \
            f"Wrong period value: {period}. Value must be between {6.58e-3} and {1.69}"

        reg_val = int((period * 152.0) - 1)
        self._blink_period[1] = (reg_val + 1) / 152.0

        self.lock.acquire()
        self.i2c_intf.write_byte_data(
            self._address, self.reg['Psc1'], reg_val)
        self.lock.release()
